const https = require("https");
const account = "ZAUB1";

https.get("https://gitlab.com/users/" + account + "/calendar.json", (resp) => {
    let data = '';

    resp.on('data', (chunk) => {
        data += chunk;
    });

    resp.on('end', () => {
        const obj = JSON.parse(data);
        var n = 0;

        Object.keys(obj).map((key) => {
            n += obj[key];
        });

        console.log(n);
    });

}).on("error", (err) => {
    console.log("Error: " + err.message);
});